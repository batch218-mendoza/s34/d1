// This code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store oure express module to variable so we could easily access its keywords, functions, and methods.
const express = require("express");

// This code creates an application using express / a.k.a express application
	// App is our server
const app = express();

// For our applications erver to run, we need a port to listen to
const port = 3000;

// For our application could read json data 
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applyhing the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{

	// This is the response that we will expect to receive if the get method with the right endpoint is successful 
	response.send("GET method success. \nHello from /hello endpoint!");

});

app.post("/hello", (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.username} succesfully registered`);
	}
	else{
		response.send("Please input BOTH username and password");
	}

});

//syntax

/*app.httpMethod("/endpoint", (request, response) = >{
	//code block
})*/


app.put("/change-password", (request, response) => {
	let message;
		//initialization // condition // change of value
	for(let i=0; i < users.length; i++){
		if (request.body.username == users[i].username){
			users[i].password = request.body.password
			message = `user ${request.body.username}'s password has been update`;
			break;

		}
		else {
			message = "user does not exist."
		}

		}
	console.log(users);
	response.send(message);
});


//1
//create a route that expects GET request at the URI/endpoint "/home"
//Response that will be received should be "Welcome to the homepage"
//code below..

app.get("/home", (request, response)=>{

	// This is the response that we will expect to receive if the get method with the right endpoint is successful 
	response.send("Welcome to the homepage");

});



//2
// create a route that expects GET request at the URI/endpoint "/users"
//Response that will be received is a collection of users
//code below..

app.get("/users", (request, response) => {
	response.send(users);
});




//3
// create a route that expects DELETE request at the URI/endpoint "/delete-user"
// the program should check if the user is existing before deleting
// response
//code below..

app.delete("/delete-user", (request, response) => {
	let message;
		//initialization // condition // change of value
	for(let i=0; i < users.length; i++){
		if (request.body.username == users[i].username){
			users[i] = users.splice[1];

			message = `user ${request.body.username}'s has been deleted`;
			break;

		}
		else {
			message = "user does not exist."
		}

		}
	console.log(users);
	response.send(message);
});



//4 
// save all the successful request tabas and export the collection 
// update your gitlab repo and link it to boodle
//code below..




// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}`))



